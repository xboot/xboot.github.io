                 xboot <http://xboot.org>


WHAT IS THE XBOOT
	xboot is the extensible bootloader for embedded system,
	based portable idea and powerful function.


XBOOT'S VERSION
	you can view the infomation of version using 'version'
	command in the xboot's shell.


XBOOT'S AUTHOR
	Jianjun Jiang <8192542@qq.com>
	see the file of CREDITS for details.


XBOOT'S COPYRIGHT
	GPLv3, see COPYING for details.


THE OFFICIAL WEBSITE
	http://xboot.org, the forum to talk about it.


XBOOT'S INTEGRATED DEVELOPMENT ENVIRONMENT
	http://pan.baidu.com/s/1i3ImG0d

THE TOOLCHAINS FOR ARM PLATFORM
	http://pan.baidu.com/s/1dDtssIt

TO COMPILE XBOOT
	just running below commands in xboot's root directory, or
	using xboot's IDE directly.
	
	make clean
	make CROSS_COMPILE=/path/to/arm-none-eabi- PLATFORM=arm32-realview

	"CROSS_COMPILE" for your cross compiler
	"PLATFORM" for your hardware machine which will be compiled


RUNNING XBOOT
	if you have no hardware platform for running xboot, you
	can using a simulator, such as qemu-system-arm in tools
	directory.

